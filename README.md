# Ultimate Calculator
#### Basic Scientific Calculator

[This software](http://minhaskamal.github.io/UltimateCalculator) has four major modes- **Simple**, **Scientific**, **Base** & **Equation**. It also has **Unit Converter**, **Date Calculator** & **Prime Number Hunter**.

### Functions
  <div align="center">
  <img title="Simple Calculator" src="https://cloud.githubusercontent.com/assets/5456665/12999507/40af666c-d17a-11e5-9a1e-fced9a7e602a.png" height="250" width=auto style="padding:5px">
  <img title="Advanced Calculator" src="https://cloud.githubusercontent.com/assets/5456665/12999505/40879e34-d17a-11e5-8f12-66467341a9e9.png" height="250" width=auto style="padding:5px">
  <img title="Base Calculator" src="https://cloud.githubusercontent.com/assets/5456665/12999508/418bccba-d17a-11e5-8c4b-c26f68a19f26.png" height="250" width=auto style="padding:5px">
  <img title="Equation Solver" src="https://cloud.githubusercontent.com/assets/5456665/12999506/40ad6d44-d17a-11e5-8215-54519cafcec1.png" height="250" width=auto style="padding:5px">
  <img title="Prime Number Hunter" src="https://cloud.githubusercontent.com/assets/5456665/16015285/cccb37d8-31b6-11e6-9955-03d1fc35eeb3.PNG" height="250" width=auto style="padding:5px">
  <img title="Date Calculator" src="https://cloud.githubusercontent.com/assets/5456665/16015284/cc9da138-31b6-11e6-9943-f0fba05521c3.PNG" height="250" width=auto style="padding:5px">
  <img title="Unit Converter" src="https://cloud.githubusercontent.com/assets/5456665/12999504/40870af0-d17a-11e5-986b-e9c09cbb4878.png" height="250" width=auto style="padding:5px">
  </div>

### Release Dates
17 Jan 2014<br/>
13 Dec 2013 (first release)

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>Ultimate Calculator is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
